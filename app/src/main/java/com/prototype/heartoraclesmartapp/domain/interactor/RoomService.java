package com.prototype.heartoraclesmartapp.domain.interactor;

import android.util.Log;
import com.prototype.heartoraclesmartapp.App;
import com.prototype.heartoraclesmartapp.data.PulseData;
import com.prototype.heartoraclesmartapp.data.PulseDataDAO;
import com.prototype.heartoraclesmartapp.data.PulseDataDB;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.reactivex.*;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.*;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.*;

public class RoomService {
    private PulseDataDAO pulseDataDao;

    public RoomService() {
        PulseDataDB db = App.getInstance().getDatabase();
        pulseDataDao = db.getPulseDataDAO();

    }

    public void setPulse(int value) {
        DateFormat date = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        Log.i("TAG", value + " текущее значение room");
        pulseDataDao.getAllNotes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<PulseData>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onSuccess(List<PulseData> pulseData) {
                        if (pulseData.size()==0){
                            Single.create(emitter -> pulseDataDao.add(new PulseData(1, date.format(Calendar.getInstance().getTime()), value)))
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribe(new SingleObserver<Object>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Object o) {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                        } else if(pulseData.size()==1){
                            Single.create(emitter -> pulseDataDao.update(new PulseData(1, date.format(Calendar.getInstance().getTime()), value)))
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribe(new SingleObserver<Object>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Object o) {
                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                        }
                    }
                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }
}

package com.prototype.heartoraclesmartapp.domain.interactor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;

import com.prototype.heartoraclesmartapp.App;
import com.prototype.heartoraclesmartapp.presentation.search.recycler.BTDevices;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class BluetoothService {
    public interface BluetoothServiceCallback{
        void recyclerCallingBack(ArrayList<BTDevices> btDevicesArrayList);
        void successConnectingCallingBack(BluetoothDevice device, String batteryLevel);
        void failedConnectingCallingBack(BluetoothDevice device);
    }
    public interface PulseServiceCallback{
        void successConnectingCallingBack(BluetoothSocket bluetoothSocket);
        void getMessageCallingBack(String message);
        void successSendingMsgCallingBack();
    }
    private BluetoothServiceCallback bluetoothServiceCallback;
    private PulseServiceCallback pulseServiceCallback;
    private BluetoothAdapter bluetoothAdapter;
    private App app;
    private UUID uuid = UUID.fromString("056776ca-8ff1-11e8-9eb6-529269fb1459");
    private static ArrayList<BTDevices> btDevicesList = new ArrayList<>();
    private static ArrayList<String> nameDevicesList = new ArrayList<>();
    private final BroadcastReceiver deviceFoundReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (nameDevicesList.contains(device.getName())){
                    btDevicesList.set(nameDevicesList.indexOf(device.getName()), new BTDevices(device.getName(), device.getAddress(), device));
                }
                else{
                    btDevicesList.add(new BTDevices(device.getName(),device.getAddress(), device));
                    nameDevicesList.add(device.getName());
                }
                //EventBus.getDefault().post(new ListEvent(btDevicesList));
                //arrayList.add(device.getName() + " " + device.getAddress());
                bluetoothServiceCallback.recyclerCallingBack(btDevicesList);
                Log.i("TAG",device.getAddress()+" "+device.getName());
                Log.i("TAG", btDevicesList.size()+"");
            }

        }
    };
    public BluetoothService(){
        app = App.getInstance();
    }

    public void registerBluetoothServiceCallBack(BluetoothServiceCallback callback){
        this.bluetoothServiceCallback = callback;
    }
    public void registerPulseServiceCallBack(PulseServiceCallback callback){
        this.pulseServiceCallback = callback;
    }
    public boolean isEnabled(){
        boolean isEnabled;
        if (app.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)){
            isEnabled = true;
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }else{
            isEnabled = false;
        }
        return isEnabled;
    }
    public void turnOn(){
        bluetoothAdapter.enable();
    }

    public void searchDevice() {
        app.registerReceiver(deviceFoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        bluetoothAdapter.startDiscovery();
    }

    public void connect(BluetoothDevice device) {
        Log.i("TAG", "touch");
        final BluetoothSocket socket;
        bluetoothAdapter.cancelDiscovery();
        BluetoothSocket presocket = null;
        try {
            presocket = device.createRfcommSocketToServiceRecord(uuid);
            Log.i("TAG", "Socket's create() method success");
        } catch (IOException e) {
            Log.i("TAG", "Socket's create() method failed", e);
        }
        socket = presocket;
        Single.create((SingleEmitter<BluetoothSocket> emitter) -> {
            Log.i("TAG", "trying to connect with "+device.getName());
            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                socket.connect();
                emitter.onSuccess(socket);
                Log.i("TAG", "Success connecting");
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                Log.i("TAG", "failed connecting");
                try {
                    socket.close();
                    emitter.onError(connectException);
                } catch (IOException closeException) {
                    Log.i("TAG", "Could not close the client socket", closeException);
                }
            }

        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new SingleObserver<BluetoothSocket>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(BluetoothSocket bluetoothSocket) {
                Log.i("TAG","OnSuccess");
                final InputStream inputStream;
                InputStream tmpIn = null;
                try {
                    tmpIn = bluetoothSocket.getInputStream();
                } catch (IOException e) {
                    Log.i("TAG", "Error occurred when creating input stream", e);
                }
                inputStream = tmpIn;
                Single.create((SingleOnSubscribe<String>) emitter -> {
                    byte[] mmBuffer = new byte[1024];
                    int numBytes = 0; // bytes returned from read()
                    while (true) {
                        try {
                            numBytes = inputStream.read(mmBuffer);
                            if (numBytes>0) {
                                String strmsg = new String(mmBuffer);
                                strmsg = strmsg.substring(0, numBytes);
                                emitter.onSuccess(strmsg);
                                Log.i("TAG",strmsg);
                                break;
                            }
                        } catch (IOException e) {
                            Log.i("TAG", "Input stream was disconnected", e);
                            break;
                        }
                    }
                }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(String s) {
                        s = s.substring(0,21);
                        if (s.equals("00")) s="100";
                        Log.i("TAG", "s="+s);
                        bluetoothServiceCallback.successConnectingCallingBack(device, s);
                        try {
                            pulseServiceCallback.successConnectingCallingBack(bluetoothSocket);
                        }catch (NullPointerException e){
                            bluetoothServiceCallback.failedConnectingCallingBack(device);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

            }

            @Override
            public void onError(Throwable e) {
                bluetoothServiceCallback.failedConnectingCallingBack(device);
            }
        });
    }
    public void sendMessage(BluetoothSocket socket, String msg){
        final OutputStream outputStream;
        OutputStream tmpOut = null;
        try {
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.e("TAG", "Error occurred when creating output stream", e);
        }
        outputStream = tmpOut;
        Single.create(emitter -> {
            try {
                outputStream.write(msg.getBytes());
                emitter.onSuccess(msg);
            } catch (IOException e) {
                Log.i("TAG", "Output stream was disconnected", e);
            }
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new SingleObserver<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Object o) {
                pulseServiceCallback.successSendingMsgCallingBack();
            }

            @Override
            public void onError(Throwable e) {

            }
        });


    }
    public void getMessage(BluetoothSocket socket){
        final InputStream inputStream;
        InputStream tmpIn = null;
        try {
            tmpIn = socket.getInputStream();
        } catch (IOException e) {
            Log.i("TAG", "Error occurred when creating input stream", e);
        }
        inputStream = tmpIn;
        io.reactivex.Observable.create((ObservableOnSubscribe<String>) emitter -> {
            byte[] mmBuffer = new byte[1024];
            int numBytes = 0; // bytes returned from read()
            while (true) {
                try {
                    numBytes = inputStream.read(mmBuffer);
                    if (numBytes>0) {
                        String strmsg = new String(mmBuffer);
                        strmsg = strmsg.substring(0, numBytes);
                        emitter.onNext(strmsg);
                    }
                } catch (IOException e) {
                    Log.i("TAG", "Input stream was disconnected", e);
                    break;
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {
                pulseServiceCallback.getMessageCallingBack(s);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                Log.i("TAG","Complete");
            }
        });

    }



}

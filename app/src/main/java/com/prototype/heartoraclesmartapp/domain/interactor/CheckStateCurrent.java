package com.prototype.heartoraclesmartapp.domain.interactor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import com.prototype.heartoraclesmartapp.R;
import com.prototype.heartoraclesmartapp.presentation.notification.StateActivity;

import androidx.core.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

public class CheckStateCurrent {
    private static final int NOTIFY_ID = 101;
    public static void Check(int pulse, Service service, boolean userReaction){
        if (pulse<45){
            sendNotification(pulse,service);
        }
        if (pulse>100 && !userReaction){
            sendNotification(pulse,service);
        }
    }
    public static void sendNotification(int pulse, Service service){
        // Create PendingIntent
        Intent posResultIntent = new Intent(service, StateActivity.class);
        posResultIntent.putExtra("result","positive" );
        PendingIntent posPendingIntent = PendingIntent.getActivity(service, 0, posResultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Intent negResultIntent = new Intent(service, StateActivity.class);
        negResultIntent.putExtra("result","negative");
        PendingIntent negPendingIntent = PendingIntent.getActivity(service, 0, negResultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
// Create Notification
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(service)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Heart Oracle")
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(BitmapFactory.decodeResource(service.getResources(),R.drawable.logo_start)))
                        .setContentText("Обнаружены нарушения сердцебиения, значения пульса лежат вне допустимых границ. "+pulse+" ударов в минуту")
                        .addAction(R.drawable.ic_launcher_background, "Вызвать помощь",
                                posPendingIntent).setAutoCancel(true).setAutoCancel(true)
                        .addAction(R.drawable.ic_launcher_background, "Игнорировать", negPendingIntent).setAutoCancel(true);
        Notification notification = builder.build();
        notification.defaults = Notification.DEFAULT_ALL;
// Show Notification
        NotificationManager notificationManager =
                (NotificationManager) service.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(2, notification);
    }
}

package com.prototype.heartoraclesmartapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.prototype.heartoraclesmartapp.App;
import com.prototype.heartoraclesmartapp.R;
import com.prototype.heartoraclesmartapp.domain.interactor.BluetoothService;
import com.prototype.heartoraclesmartapp.domain.interactor.CheckStateCurrent;
import com.prototype.heartoraclesmartapp.domain.interactor.RoomService;
import com.prototype.heartoraclesmartapp.presentation.PulseDisplayActivity;

import androidx.core.app.NotificationCompat;

public class MainService extends Service implements BluetoothService.PulseServiceCallback {
    private BluetoothService bluetoothService;
    public static boolean userReaction = false;
    RoomService roomService = new RoomService();
    public void onCreate() {
        bluetoothService = App.getInstance().getBluetoothService();
        bluetoothService.registerPulseServiceCallBack(this);
        Intent resultIntent = new Intent(this, PulseDisplayActivity.class);
        resultIntent.putExtra("result","nothing");
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Heart Oracle")
                        .setContentIntent(resultPendingIntent)
                        .setContentText("Запущено приложение");

        Notification notification = builder.build();
        startForeground(1, notification);
        Log.i("TAG", "Create service");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {

        return null;
    }
    @Override
    public void successConnectingCallingBack(BluetoothSocket bluetoothSocket) {
        Log.i("TAG", "Service got callback and sent message");
        bluetoothService.getMessage(bluetoothSocket);
    }

    @Override
    public void getMessageCallingBack(String message) {
        Log.i("TAG", message);
        int pulse=-1;
        int batteryLvl=0;
        for (int i=6; i<message.length();i++){
            if (message.charAt(i)==' ' && pulse==-1){
                pulse = Integer.parseInt(message.substring(6, i));
                continue;
            }
            if (message.charAt(i)==' ' && pulse!=-1){
                try {
                    batteryLvl=Integer.parseInt(message.substring(i+1,i+3));
                    if (batteryLvl==0){
                        batteryLvl=100;
                    }
                }catch (NumberFormatException e){
                    batteryLvl=0;
                }
                break;
            }
        }
        Log.i("TAG", pulse+"  "+batteryLvl);
        roomService.setPulse(pulse);
        CheckStateCurrent.Check(pulse, this, userReaction);
    }

    @Override
    public void successSendingMsgCallingBack() {
        Log.i("TAG", "Success sending message");
    }

}

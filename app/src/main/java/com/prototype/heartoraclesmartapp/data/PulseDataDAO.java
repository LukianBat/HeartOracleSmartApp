package com.prototype.heartoraclesmartapp.data;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.*;

@Dao
public interface PulseDataDAO {

    @Insert
    void add(PulseData pulseData);

    @Insert
    void insertAll(PulseData... pulseData);

    @Update
    void update(PulseData pulseData);

    @Delete
    void delete(PulseData pulseData);

    @Query("SELECT * FROM pulsedata")
    Single <List<PulseData>> getAllNotes();

    @Query("SELECT * FROM pulsedata WHERE id = :id")
    PulseData getNoteById(int id);

}

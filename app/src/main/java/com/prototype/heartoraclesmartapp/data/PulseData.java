package com.prototype.heartoraclesmartapp.data;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class PulseData {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String date;
    public int current;

    public PulseData(int id, String date, int current) {
        this.id = id;
        this.date = date;
        this.current = current;
    }
}

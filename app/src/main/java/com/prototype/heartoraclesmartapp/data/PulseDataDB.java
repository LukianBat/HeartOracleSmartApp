package com.prototype.heartoraclesmartapp.data;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {PulseData.class}, version = 1)
public abstract class PulseDataDB extends RoomDatabase {
    public abstract PulseDataDAO getPulseDataDAO();
}

package com.prototype.heartoraclesmartapp.presentation.notification;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.heartoraclesmartapp.R;
import com.prototype.heartoraclesmartapp.MainService;


public class NegativeAnswerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainService.userReaction=true;
        Log.i("TAG", "Реакция пользователя: "+MainService.userReaction+"");
        return inflater.inflate(R.layout.fragment_negative_answer, container, false);
    }
}

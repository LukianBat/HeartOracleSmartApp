package com.prototype.heartoraclesmartapp.presentation.successed;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.prototype.heartoraclesmartapp.R;
import com.prototype.heartoraclesmartapp.presentation.notification.StateActivity;

import static androidx.navigation.Navigation.findNavController;

public class SuccessConnectingFragment extends Fragment {

    public SuccessConnectingFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_success_connecting, container, false);
        Button button = view.findViewById(R.id.button_ok);
        TextView textView = view.findViewById(R.id.textView_success_connected);
        textView.append(getArguments().getString("nameDevice"));
        textView.append("\n"+getArguments().getString("deviceClass"));
        textView.append("\n"+getArguments().getString("batteryLevel"));
        button.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(),StateActivity.class);
            intent.putExtra("result","nothing");
            startActivity(intent);
            button.setVisibility(View.GONE);
        });
        return view;
    }
}

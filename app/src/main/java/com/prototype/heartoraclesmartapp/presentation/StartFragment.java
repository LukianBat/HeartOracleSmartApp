package com.prototype.heartoraclesmartapp.presentation;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.prototype.heartoraclesmartapp.R;

import static androidx.navigation.Navigation.findNavController;


public class StartFragment extends Fragment {
    public StartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        NavController navController = findNavController(getActivity(), R.id.my_nav_host_fragment);
        navController.navigate(R.id.action_startFragment_to_connectingFragment);
        return inflater.inflate(R.layout.fragment_start, container, false);
    }

}

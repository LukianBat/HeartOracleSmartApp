package com.prototype.heartoraclesmartapp.presentation.search;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;

import androidx.navigation.NavController;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.prototype.heartoraclesmartapp.R;
import com.prototype.heartoraclesmartapp.MainService;
import com.prototype.heartoraclesmartapp.presentation.search.recycler.RecyclerViewBTAdapter;

import static androidx.navigation.Navigation.findNavController;


public class ConnectingFragment extends MvpAppCompatFragment implements ConnectingFragmentView {

    private static final int DISCOVERY_REQUEST = 123;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    @InjectPresenter
    ConnectingFragmentPresenter connectingFragmentPresenter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connecting, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        progressBar = view.findViewById(R.id.progressBar);
        getActivity().startService(new Intent(getActivity(),MainService.class));
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("TAG", "onResult " + requestCode + " " + resultCode);
        if (resultCode>0) {
            connectingFragmentPresenter.searchDevice();
        }
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(getContext(), text,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setBTVisible() {
        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE), DISCOVERY_REQUEST);
    }
    @Override
    public void updateRecycler(RecyclerViewBTAdapter adapter) {
        Log.i("TAG", "update recycler");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(itemAnimator);
    }

    @Override
    public void nextFragment(boolean success, Bundle bundle) {
        if (success) {
            //progressBar.setVisibility(View.INVISIBLE);
            findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_connectingFragment_to_successConnectingFragment,bundle);
        }else {
            //progressBar.setVisibility(View.INVISIBLE);
            getActivity().stopService(new Intent(getActivity(), MainService.class));
            NavController navController = findNavController(getActivity(), R.id.my_nav_host_fragment);
            navController.navigate(R.id.action_connectingFragment_to_failedConnectingFragment, bundle);
        }
    }
    @Override
    public void unclickableRecycler() {
        recyclerView.setClickable(false);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }
}

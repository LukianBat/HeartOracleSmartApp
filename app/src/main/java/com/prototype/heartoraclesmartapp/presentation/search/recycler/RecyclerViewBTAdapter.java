package com.prototype.heartoraclesmartapp.presentation.search.recycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prototype.heartoraclesmartapp.App;
import com.prototype.heartoraclesmartapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewBTAdapter extends RecyclerView.Adapter<RecyclerViewBTAdapter.MyViewHolder> {
    private List<BTDevices> devicesList;
    public interface RecyclerCallback{
        void onItemClick();
    }
    private RecyclerCallback recyclerCallback;
    public RecyclerViewBTAdapter(List<BTDevices> devicesList) {
        this.devicesList = devicesList;

    }
    public void registerRecyclerCallback(RecyclerCallback recyclerCallback){
        this.recyclerCallback = recyclerCallback;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.device_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final BTDevices btdevice = devicesList.get(position);

        holder.nameTextView.setText(btdevice.getName());
        holder.adressTextView.setText(btdevice.getAdress());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerCallback.onItemClick();
                App.getInstance().getBluetoothService().connect(btdevice.getDevice());
            }
        });
    }
    @Override
    public int getItemCount() {
        return devicesList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView adressTextView;

        MyViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.textViewLarge);
            adressTextView = itemView.findViewById(R.id.textViewSmall);
        }
    }
}
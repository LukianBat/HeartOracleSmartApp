package com.prototype.heartoraclesmartapp.presentation;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import android.os.Bundle;
import android.util.Log;

import com.prototype.heartoraclesmartapp.R;

import static androidx.navigation.Navigation.findNavController;


public class MainActivity extends AppCompatActivity {
    private NavController navController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navController = findNavController(this, R.id.my_nav_host_fragment);

    }
    @Override
    public void onBackPressed() {
        // do nothing.
    }



}

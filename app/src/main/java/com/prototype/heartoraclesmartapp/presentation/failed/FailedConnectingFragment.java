package com.prototype.heartoraclesmartapp.presentation.failed;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.prototype.heartoraclesmartapp.R;

import static androidx.navigation.Navigation.findNavController;

public class FailedConnectingFragment extends Fragment {
    public FailedConnectingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_failed_connecting, container, false);
        Button button = view.findViewById(R.id.button_recon);
        TextView textView = view.findViewById(R.id.textView_failed);
        textView.append(getArguments().getString("nameDevice")+"\nПопробуйте подключиться ещё раз.");
        button.setOnClickListener(view1 -> {
            NavController navController = findNavController(getActivity(), R.id.my_nav_host_fragment);
            navController.navigate(R.id.action_failedConnectingFragment_to_connectingFragment);
        });


        return view;
    }

}

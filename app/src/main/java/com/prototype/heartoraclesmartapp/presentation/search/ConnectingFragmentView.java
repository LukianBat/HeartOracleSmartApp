package com.prototype.heartoraclesmartapp.presentation.search;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;

import com.arellomobile.mvp.MvpView;
import com.prototype.heartoraclesmartapp.presentation.search.recycler.RecyclerViewBTAdapter;
public interface ConnectingFragmentView extends MvpView {
    void showToast(String text);
    void setBTVisible();
    void updateRecycler(RecyclerViewBTAdapter adapter);
    void nextFragment(boolean success,Bundle bundle);
    void unclickableRecycler();
}
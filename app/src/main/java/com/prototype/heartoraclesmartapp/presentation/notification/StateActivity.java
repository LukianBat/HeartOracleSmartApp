package com.prototype.heartoraclesmartapp.presentation.notification;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.prototype.heartoraclesmartapp.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;

import static androidx.navigation.Navigation.findNavController;

public class StateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state);
        Log.i("TAG1","onCreate");
        Intent intent = getIntent();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(2);
        String result = intent.getStringExtra("result");
        Log.i("TAG1","onCreate res "+result);
        if (result.equals("positive")){
            NavController navController = findNavController(this, R.id.my_nav_host_fragment_state);
            navController.navigate(R.id.action_emptyStateFragment_to_positiveAnswerFragment);
        }
        if (result.equals("negative")){
            NavController navController = findNavController(this, R.id.my_nav_host_fragment_state);
            navController.navigate(R.id.action_emptyStateFragment_to_negativeAnswerFragment);
        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }

}

package com.prototype.heartoraclesmartapp.presentation.search;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.prototype.heartoraclesmartapp.App;
import com.prototype.heartoraclesmartapp.domain.interactor.BluetoothService;
import com.prototype.heartoraclesmartapp.presentation.search.recycler.BTDevices;
import com.prototype.heartoraclesmartapp.presentation.search.recycler.RecyclerViewBTAdapter;

import java.util.ArrayList;


import static android.bluetooth.BluetoothClass.Device.Major.AUDIO_VIDEO;
import static android.bluetooth.BluetoothClass.Device.Major.COMPUTER;
import static android.bluetooth.BluetoothClass.Device.Major.HEALTH;
import static android.bluetooth.BluetoothClass.Device.Major.IMAGING;
import static android.bluetooth.BluetoothClass.Device.Major.MISC;
import static android.bluetooth.BluetoothClass.Device.Major.NETWORKING;
import static android.bluetooth.BluetoothClass.Device.Major.PERIPHERAL;
import static android.bluetooth.BluetoothClass.Device.Major.UNCATEGORIZED;
import static android.bluetooth.BluetoothClass.Device.Major.WEARABLE;
import static android.provider.ContactsContract.DisplayNameSources.PHONE;

@InjectViewState
public class ConnectingFragmentPresenter extends MvpPresenter<ConnectingFragmentView> implements BluetoothService.BluetoothServiceCallback,RecyclerViewBTAdapter.RecyclerCallback {
    private BluetoothService bluetoothService;
    ConnectingFragmentPresenter(){
        bluetoothService = App.getInstance().getBluetoothService();
        bluetoothService.registerBluetoothServiceCallBack(this);
        if (bluetoothService.isEnabled()){
            bluetoothService.turnOn();
            getViewState().showToast("Bluetooth module have activated!");
            getViewState().setBTVisible();
        }else{
            getViewState().showToast("Bluetooth module haven't been detected!");
        }
    }
    void searchDevice(){
        bluetoothService.searchDevice();

    }

    @Override
    public void recyclerCallingBack(ArrayList<BTDevices> btDevicesArrayList) {
        RecyclerViewBTAdapter adapter = new RecyclerViewBTAdapter(btDevicesArrayList);
        adapter.registerRecyclerCallback(this);
        getViewState().updateRecycler(adapter);

    }

    @Override
    public void successConnectingCallingBack(BluetoothDevice bluetoothDevice, String batteryLevel) {
        Bundle bundle = new Bundle();
        bundle.putString("batteryLevel",batteryLevel);
        bundle.putString("nameDevice", bluetoothDevice.getName());
        switch (bluetoothDevice.getBluetoothClass().getMajorDeviceClass()){
            case AUDIO_VIDEO:{
                bundle.putString("deviceClass", "Вы подключены к AUDIO-VIDEO аппаратуре, советуем подключиться к устройству с датчиком пульса");
            }
            case COMPUTER:{
                bundle.putString("deviceClass", "Вы подключены к компьютеру, советуем подключиться к устройству с датчиком пульса");
            }
            case HEALTH:{
                bundle.putString("deviceClass", "Вы подключены к HEALTH устройству");
            }
            case IMAGING:{
                bundle.putString("deviceClass", "Вы подключены к IMAGING устройству, советуем подключиться к устройству с датчиком пульса");
            }
            case MISC:{
                bundle.putString("deviceClass", "Вы подключены к MISC устройству, советуем подключиться к устройству с датчиком пульса");
            }
            case NETWORKING:{
                bundle.putString("deviceClass", "Вы подключены к NETWORKING устройству, советуем подключиться к устройству с датчиком пульса");
            }
            case PERIPHERAL:{
                bundle.putString("deviceClass", "Вы подключены к PERIPHERAL устройству, советуем подключиться к устройству с датчиком пульса");
            }
            case PHONE:{
                bundle.putString("deviceClass", "Вы подключены к смартфону, советуем подключиться к устройству с датчиком пульса");
            }
            case UNCATEGORIZED:{
                bundle.putString("deviceClass", "Вы подключены к неопознанному устройству, советуем подключиться к устройству с датчиком пульса");
            }
            case WEARABLE:{
                bundle.putString("deviceClass", "Вы подключены к WEARABLE устройству");
            }
        }
        getViewState().nextFragment(true, bundle);

    }
    @Override
    public void failedConnectingCallingBack(BluetoothDevice bluetoothDevice) {
        Bundle bundle = new Bundle();
        bundle.putString("nameDevice", bluetoothDevice.getName());
        getViewState().nextFragment(false, bundle);
    }

    @Override
    public void onItemClick() {
        getViewState().unclickableRecycler();
    }
}
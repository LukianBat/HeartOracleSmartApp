package com.prototype.heartoraclesmartapp.presentation;


import android.os.Bundle;

import com.arellomobile.mvp.MvpActivity;
import com.prototype.heartoraclesmartapp.R;

public class PulseDisplayActivity extends MvpActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse_display);
    }
}

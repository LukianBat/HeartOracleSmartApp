package com.prototype.heartoraclesmartapp;

import android.app.Application;
import com.prototype.heartoraclesmartapp.data.PulseDataDB;
import com.prototype.heartoraclesmartapp.domain.interactor.BluetoothService;
import androidx.room.Room;

public class App extends Application {

    public static App instance;

    private PulseDataDB database;
    private BluetoothService bluetoothService;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, PulseDataDB.class, "database").build();
        bluetoothService =  new BluetoothService();
    }

    public static App getInstance() {
        return instance;
    }

    public BluetoothService getBluetoothService() {
        return bluetoothService;
    }

    public PulseDataDB getDatabase() {
        return database;
    }

}